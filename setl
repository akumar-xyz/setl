#!/usr/bin/env python

import re
import sys
import fileinput


if '-h' in sys.argv or '--help' in sys.argv:
    HELP_STR = """\
Usage: {0} [OPTION]... [FILE]...
A simple command line tool to settle shared expenses.

With no FILE, or when FILE is -, read standard input.

Options:
-h, --help         display this help and exit
-v, --version      display version and exit

""".format(sys.argv[0])
    print(HELP_STR)
    sys.exit(0)

if '-v' in sys.argv or '--version' in sys.argv:
    VERSION = '007'
    print(VERSION)
    sys.exit(0)


class Dude:
    """ Person we are sharing expenses with  """

    def __init__(self, name):
        self.name = name
        self.balance = 0
        self.total_payed = 0
        self.expenses = 0

    def add_expense(self, amount):
        """Adds expenditure

        :amount: float
        :returns: None

        """
        self.expenses += amount

    def add_to_balance(self, amount):
        """Adds amount balance

        :amount: float
        :returns: None

        """
        self.balance += amount

    def add_to_payed(self, amount):
        """Adds amount payed

        :amount: float
        :returns: None

        """

        self.total_payed += amount

    def get_name(self):
        """Returns the name of the Dude
        :returns: string

        """
        return self.name

    def get_balance(self):
        """gets balance amount
        :returns: float

        """
        return self.balance

    def __str__(self):
        obj_str = ""
        obj_str += self.name + ":\n"
        obj_str += "Individual Expenses\t:\t%.2f\n" % (self.expenses)
        obj_str += "Total Payed\t:\t%.2f\n" % (self.total_payed)
        if self.balance > 0:
            obj_str += "Should get\t:\t%.2f\n" % (self.balance)
        else:
            obj_str += "Should pay\t:\t%.2f\n" % (-self.balance)

        return obj_str


class Expense:
    """ Each Expense is an Object """

    def __init__(self, description, payments, shared_among):

        self.description = description
        self.payments = payments
        self.shared_among = shared_among
        
        self.total_money_spent = 0

        for money_spent in payments.values():
            self.total_money_spent += money_spent

        self.expenses_per_person = self.total_money_spent/len(shared_among)

        self.balances = {}
        self.balances.update(payments)

        for dude in shared_among:
            peep_balance = payments.get(dude, 0) - self.expenses_per_person
            self.balances[dude] = peep_balance

        self.expenses = {}

        for dude in shared_among:
            self.expenses[dude] = self.expenses_per_person


    def get_shared_among(self):
        return self.shared_among

    def get_description(self):
        return self.description

    def get_balances(self):
        return self.balances

    def get_individual_expenses(self):
        return self.expenses

    def get_expense_per_person(self):
        return self.expenses_per_person

    def get_payments(self):
        return self.payments


class RecordsBook:
    """ Class that keeps track of all expenses and 'Dudes' """

    def __init__(self, list_of_people):
        self.dudes = {}
        self.list_expenses = []
        for dude in list_of_people:
            self.dudes[dude] = Dude(dude)

    def add_dude(self, dude_name):
        """Adds a person to the RecordsBook

        :dude_name: string
        :returns: None

        """
        if dude_name not in self.dudes:
            self.dudes[dude_name] = Dude(dude_name)

    def add_expense(self, description, payments, shared_among):
        """Adds an expense in the RecordsBook

        :description: string
        :payments: dict{ string: float }
        :returns: none

        """

        for peep_name, payed in payments.items():
            self.dudes[peep_name].add_to_payed(payed)

        self.list_expenses.append(Expense(description, payments, shared_among))

    def calculate_individual_expenses(self):
        """Calculate expenses for each 'Dude'
        :returns: None

        """

        for expense in self.list_expenses:
            balances = expense.get_balances()
            expenses = expense.get_individual_expenses()

            for peep_name, balance in balances.items():
                dude = self.dudes[peep_name]
                dude.add_to_balance(balance)

            for peep_name, amount in expenses.items():
                dude = self.dudes[peep_name]
                dude.add_expense(amount)


    def get_debts(self):
        """TODO: Docstring for get_debts.
        :returns: TODO

        """
        self.calculate_individual_expenses()
        list_of_debts = []
        list_of_debtors = []
        list_of_creditors = []

        for dude in self.dudes.values():
            peep_name = dude.get_name()
            to_get = dude.get_balance()

            if to_get < 0:
                list_of_debtors.append((peep_name, round(-to_get, 2)))
            else:
                list_of_creditors.append((peep_name, round(to_get, 2)))

        list_of_creditors.sort(key=lambda x: x[1])
        list_of_debtors.sort(key=lambda x: x[1])
        while list_of_creditors or list_of_debtors:

            try:
                should_give = list_of_debtors.pop()
                should_get = list_of_creditors.pop()

                to_give = min(should_give[1], should_get[1])

                list_of_debts.append((should_give[0], should_get[0], to_give))

                if should_give[1] == should_get[1]:
                    continue

                if to_give == should_give[1]:
                    list_of_creditors.append(
                        (should_get[0], should_get[1]-to_give))
                else:
                    list_of_debtors.append(
                        (should_give[0], should_give[1]-to_give))

            except IndexError:
                if list_of_creditors:
                    if list_of_creditors[-1][1] < 1:
                        list_of_creditors.pop()
                        continue
                if list_of_debtors:
                    if list_of_debtors[-1][1] < 1:
                        list_of_debtors.pop()
                        continue

        return list_of_debts

    def display_settlement(self):
        """Ugly logic to print all the expense details and settlements

        :returns: None

        """
        debts = (self.get_debts())

        print("Expenses")
        print("========\n")

        for expense in self.list_expenses:

            print("== " + expense.get_description())

            payments = expense.get_payments()

            for dude_name, spent in payments.items():
                if spent > 0:
                    print("\t%s payed \t%.2f"%(dude_name, spent))

            print("\n\tShared among :", end=" ")

            shared_among = expense.get_shared_among()

            if len(shared_among) == len(self.dudes):
                print("\tEveryone")
            else:
                print(", ".join(shared_among))

            print("\t(%.2f) per person" % (expense.get_expense_per_person()))
            print()

        print("Individual Expenses")
        print("===================\n")
        for dude in self.dudes.values():
            print("== ", str(dude))

        print()
        print("Settlements")
        print("===========\n")
        for d in debts:
            print("%s --> %s: %.2f" % (d[0], d[1], d[2]))


class DSL(object):

    """ Domain Specific Language Parser Class """

    def __init__(self):

        # Objects
        self.rec = RecordsBook([])
        self.dudes = set()
        self.aliases = dict()

        # Regex

        # Comment or empty line
        self.re_comment = re.compile(r"^\s*(#.*)?$")

        # Statement to add a person
        self.re_add_dude = re.compile(r'^\s*\+\s*(?P<dudename>[A-Za-z_]+)')

        # Statement to remove a person
        self.re_rem_dude = re.compile(r'^\s*-\s*(?P<dudename>[A-Za-z_]+)')

        # Statement to define an alias
        self.re_alias = re.compile(r'^\s*(?P<alias>[A-Za-z_]+)\s*\((?P<list>[^\)]+)\)')

        # Statement to add an expense
        self.re_add_expense =\
                re.compile(r'^(?P<payments>[^/]*)(/(?P<dudes>.*))?@(?P<desc>.*)$')

        # Regex to parse a payment
        self.re_payment = re.compile(r'(?P<dudename>[A-Za-z_]+)\s*(?P<t>[:!])\s*(?P<amount>[0-9]+(\.[0-9]+)?)')


    def add_dude(self, name):
        self.dudes.add(name)

    def rem_dude(self, name):
        self.dudes.remove(name)

    def parse_list(self, comma_sapareted_str):
        return set(map(str.strip, comma_sapareted_str.split(',')))

    def add_expense(self, payments, dudes, description):

        # Process list
        if dudes is not None:
            shared_among = set()
            for name in self.parse_list(dudes):
                if name in self.aliases:
                    shared_among = shared_among.union(self.aliases.get(name))
                else:
                    shared_among.add(name)
            dudes = shared_among
        else:
            dudes = self.dudes.copy()

        # Process payments
        payments_dict = dict()
        for p in self.parse_list(payments):
            match = self.re_payment.match(p)
            name = match.group('dudename')
            amount = float(match.group('amount'))
            payments_dict[name] = amount

            t = match.group('t')
            if t == ":":
                dudes.add(name)
            elif t == "!" and name in dudes:
                dudes.remove(name)

        # Process description
        description = description.strip()

        self.rec.add_expense(description, payments_dict, dudes)


    def add_alias(self, alias_name, alias_list):
        alias_list = self.parse_list(alias_list)
        self.aliases[alias_name] = set(alias_list)

    def parse(self):

        for line in fileinput.input():
            try:

                # Skip line if comment or empty
                if self.re_comment.match(line) is not None:
                    continue

                # Add a 'dude' to the record book object
                match = self.re_add_dude.match(line)
                if match:
                    dude_name = match.group('dudename')
                    self.add_dude(dude_name)
                    self.rec.add_dude(dude_name)
                    continue

                # Add an alias
                match = self.re_alias.match(line)
                if match:
                    alias_name = match.group(1)
                    alias_list = match.group(2)
                    self.add_alias(alias_name, alias_list)
                    continue

                # Remove a 'dude' to the record book object
                match = self.re_rem_dude.match(line)
                if match:
                    dude_name = match.group('dudename')
                    self.rem_dude(dude_name)
                    continue

                # Add an expense
                match = self.re_add_expense.match(line)
                if match:
                    payments = match.group('payments')
                    shared_among = match.group('dudes')
                    description = match.group('desc')
                    self.add_expense(payments, shared_among, description)
                    continue


            except AttributeError:
                print("Error parsing at line : ", fileinput.lineno(),
                      file=sys.stderr)
                print(line, file=sys.stderr)
                sys.exit(-1)

            except KeyError as e:
                print("Error parsing at line : ", fileinput.lineno(),
                      file=sys.stderr)
                print("Wait, who is '%s'?" % e.args[0], file=sys.stderr)
                sys.exit(-1)

            except Exception as e:
                raise e



setl = DSL()
try:
    setl.parse()
    setl.rec.display_settlement()
except KeyError as e:
    print("Error parsing at line : ", fileinput.lineno(),
          file=sys.stderr)
    print("Wait, who is '%s'?" % e.args[0], file=sys.stderr)
    sys.exit(-1)
except Exception as e:
    print("Error: ", e)
    sys.exit(-1)
