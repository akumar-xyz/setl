Expenses
========

== Train Fare
	Botvinnik payed 	130.00

	Shared among : Karpov, Carlsen, Kasparov, Botvinnik, Anand
	(26.00) per person

== Food
	Anand payed 	100.00
	Carlsen payed 	100.00

	Shared among : Karpov, Carlsen, Kasparov, Botvinnik, Anand
	(40.00) per person

== Vodka
	Fischer payed 	150.00

	Shared among : Kasparov, Karpov, Fischer, Botvinnik
	(37.50) per person

== Dinner
	Karpov payed 	225.00

	Shared among : 	Everyone
	(37.50) per person

== Cab
	Carlsen payed 	70.00

	Shared among : Botvinnik, Anand
	(35.00) per person

== Hat Shop
	Karpov payed 	20.00
	Fischer payed 	30.00

	Shared among : Carlsen
	(50.00) per person

Individual Expenses
===================

==  Anand:
Individual Expenses	:	138.50
Total Payed	:	100.00
Should pay	:	38.50

==  Karpov:
Individual Expenses	:	141.00
Total Payed	:	245.00
Should get	:	104.00

==  Kasparov:
Individual Expenses	:	141.00
Total Payed	:	0.00
Should pay	:	141.00

==  Carlsen:
Individual Expenses	:	153.50
Total Payed	:	170.00
Should get	:	16.50

==  Botvinnik:
Individual Expenses	:	176.00
Total Payed	:	130.00
Should pay	:	46.00

==  Fischer:
Individual Expenses	:	75.00
Total Payed	:	180.00
Should get	:	105.00


Settlements
===========

Kasparov --> Fischer: 105.00
Kasparov --> Karpov: 36.00
Botvinnik --> Karpov: 46.00
Anand --> Karpov: 22.00
Anand --> Carlsen: 16.50
